﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ToDoApp
{
    class Movie
    {
        public string Title { get; set; }
        public string Year { get; set; }
        public string Rating { get; set; }
        public string Genre { get; set; }

        public Movie(string title, string year, string rating, string genre)
        {
            Title = title;
            Year = year;
            Rating = rating;
            Genre = genre;
        }

        public Movie()
        {
        }
    }

    class MovieCatalog
    {
        public static void MoviesProgram()
        {
            bool runProgram = true;
            string filePath = "Movies.csv";
            List<Movie> movies = new List<Movie>();
            string choice = "";

            //Read the tasks from the file, and if there are no such file create a new
            try
            {
                using (StreamReader sr = new StreamReader(filePath))
                {
                    using (var csv = new CsvReader(sr, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        movies = csv.GetRecords<Movie>().ToList();
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    Console.WriteLine("File Not found, created a new");
                }
            }

            Console.WriteLine("To exit type \"e\"");
            Console.WriteLine("To print type \"p\"");
            Console.WriteLine("To add a new task type \"a\" and the task to add");

            while (runProgram)
            {
                choice = Console.ReadLine();
                switch (choice[0])
                {
                    case 'e':
                        runProgram = false;
                        break;
                    case 'a':
                        movies = AddMovieReview(movies, filePath);
                        break;
                    case 'p':
                        movies = PrintMovies(movies, filePath);
                        break;
                    default:
                        break;
                }
            }
        }

        //Prints all the tasks stored in the file
        private static List<Movie> PrintMovies(List<Movie> movies, string filePath)
        {
            foreach(Movie movie in movies)
            {
                Console.WriteLine($"movie: {movie.Title}, genre: {movie.Genre}, year: {movie.Year}, rating: {movie.Rating}");
            }
            return movies;
        }

        private static List<Movie> AddMovieReview(List<Movie> movies, string filePath)
        {
            Console.WriteLine("Input: \"movie\" \"year\" \"rating\" \"genre\"");
            string[] arguments = Console.ReadLine().Split(" ");
            Movie newMovie = new Movie(arguments[0],
                                       arguments[1],
                                       arguments[2],
                                       arguments[3]);
            movies.Add(newMovie);

            using (StreamWriter sw = new StreamWriter(filePath))
            {
                using (var csv = new CsvWriter(sw, System.Globalization.CultureInfo.CurrentCulture))
                {
                    csv.WriteRecords(movies);
                }
            }

            return movies;
        }
            
    }

}
