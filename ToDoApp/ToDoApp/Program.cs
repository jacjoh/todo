﻿using System;

namespace ToDoApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string choice = "";
            bool runProgram = true;
            while (runProgram)
            {
                Console.WriteLine("- To exit program type \"e\"");
                Console.WriteLine("- To check your to do tasks type \"todo\"");
                Console.WriteLine("- To check movie reviews type \"movie\"");
                choice = Console.ReadLine();
                switch (choice)
                {
                    case "e":
                        runProgram = false;
                        break;
                    case "todo":
                        ToDo.ToDoProgram();
                        break;
                    case "movie":
                        MovieCatalog.MoviesProgram();
                        break;
                    default:
                        break;
                }
            }
            Console.WriteLine("Hello World!");
        }
    }
}
