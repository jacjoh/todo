﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Text;

namespace ToDoApp
{
    class ToDo
    {
        public static void ToDoProgram()
        {
            bool runProgram = true;
            string filePath = "Tasks.txt";
            string toDoList = "";
            string choice = "";

            //Read the tasks from the file, and if there are no such file create a new
            try
            {
                using (StreamReader sr = new StreamReader(filePath))
                {
                    toDoList = sr.ReadToEnd();
                }
            }
            catch (FileNotFoundException ex) 
            {
                Console.WriteLine(ex.Message);
                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    Console.WriteLine("File Not found, created a new");
                }
            }

            Console.WriteLine("To exit type \"e\"");
            Console.WriteLine("To print type \"p\"");
            Console.WriteLine("To add a new task type \"a\" and the task to add");

            while (runProgram)
            {
                choice = Console.ReadLine();
                switch (choice[0])
                {
                    case 'e':
                        runProgram = false;
                        break;
                    case 'a':
                        toDoList = AddTask(filePath, choice.Substring(choice.IndexOf(" ") + 1), toDoList);
                        break;
                    case 'p':
                        toDoList = PrintTasks(filePath, toDoList);
                        break;
                    default:
                        break;
                }
            }
        }
        //Prints all the tasks stored in the file
        private static string PrintTasks(string filePath, string toDoList)
        { 
            using (StreamReader sr = new StreamReader(filePath))
            {
                toDoList = sr.ReadToEnd();
            }
            Console.WriteLine(toDoList);
            return toDoList;
        }

        //adds a new task to the file
        private static string AddTask(string filePath, string task, string toDoList)
        {
            toDoList += "- " + task;
            using(StreamWriter sw = new StreamWriter(filePath))
            {
                sw.WriteLine(toDoList);
            }

            return toDoList;
        } 
    }
}
